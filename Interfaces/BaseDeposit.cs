﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    
     public class BaseDeposit : Deposit
    {
        public BaseDeposit(decimal amount, int period) : base(amount, period)
        {
        }

        public override decimal Income()
        {
            double monthIncomePercent = 0.05;
            
            var income = Amount * (decimal) Math.Pow(1 + monthIncomePercent, Period) - Amount;

            return income;
        }
    }

}
