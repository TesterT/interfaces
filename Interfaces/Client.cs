﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Interfaces
{
    
    public class Client : IEnumerable<Deposit>
    {
        private Deposit[] deposits;

        public Client()
        {
            deposits = new Deposit[10];
            
        }

        public bool AddDeposit(Deposit deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i] == null)
                {
                    deposits[i] = deposit;

                    return true;
                }
            }
            return false;
        }

        public decimal TotalIncome()
        {
            return deposits.Where(d => d != null).Sum(d => d.Income());
        }

        public decimal MaxIncome()
        {
            return deposits.Where(d => d != null).Max(d => d.Income());
        }

        public decimal GetIncomeByNumber(int number)
        {
            if (deposits[--number] != null)
            {
                return deposits[number].Income();
            }

            return 0;
        }

        public void SortDeposits()
        {
            //RETEST ============================
            /*var list = deposits.ToList();
            list.Sort((x, y) => y.CompareTo(x));
            deposits = list.ToArray();*/
            var notNullElementsCount = deposits.Count(element => element != null);

            Array.Sort(deposits, 0, notNullElementsCount);

            Array.Reverse(deposits, 0, notNullElementsCount);

        }

        public int CountPossibleToProlongDeposit()
        {
            /*return deposits.Where(x =>
                {
                    if (x is IProlongable prolongable)
                        return prolongable.CanToProlong();

                    return false;
                })
                .Count();*/
                
            int counter = 0;
           
            foreach (Deposit dep in deposits)
            {
                /*if (dep != null)
                {*/
                    
                    if (dep is IProlongable prolongable)
                    {
                        if (prolongable.CanToProlong())
                        {
                            counter++;
                        } 
                    }
                /*}*/
            }
            return counter;
        }

        public IEnumerator<Deposit> GetEnumerator()
        {
            return deposits.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
            
        }
    }

}
