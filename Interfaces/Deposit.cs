﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics.CodeAnalysis;

namespace Interfaces
{
   
    public abstract class Deposit : IComparable<Deposit>
    {
        public decimal Amount { get; }
        public int Period { get; }

        public Deposit(decimal amount, int period)
        {
            Amount = amount;
            Period = period;
        }

        public abstract decimal Income();

        public int CompareTo( Deposit other)
        {
            var sum = Amount + Income();                //RETEST Amount + Income()
            var otherSum = other.Amount + other.Income();     //RETEST other.Amount + other.Income()

            return sum.CompareTo(otherSum);
        }
    }

}
