﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    
    public class LongDeposit : Deposit, IProlongable
    {
        public LongDeposit(decimal amount, int period) : base(amount, period)
        {
        }

        public bool CanToProlong()
        {
            return Period <= 36;       
            
        }

        public override decimal Income()
        {
            var monthWhenIncomeStarts = 6;
            if (Period > monthWhenIncomeStarts)
            {
                double monthIncomePercent = 0.15;
            
                var income = Amount * (decimal) Math.Pow(1 + monthIncomePercent, Period - monthWhenIncomeStarts) - Amount;

                return income;
            }

            return 0;
        }
    }

}
