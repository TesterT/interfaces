﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    
    public class SpecialDeposit : Deposit, IProlongable
    {
        public SpecialDeposit(decimal amount, int period) : base(amount, period)
        {
        }

        public bool CanToProlong()
        {
            return Amount > 1000;
            
        }

        public override decimal Income()
        {
            decimal income = Amount;

            for (int i = 1; i <= Period; i++)
            {
                var monthIncomePercent = (decimal) i / 100;

                income += income *  monthIncomePercent;
            }
            
            return income - Amount;
        }
    }

}
